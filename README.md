Link al informe: https://docs.google.com/document/d/122vlk-gywiJ9h1yJUGW4vzAywoK0E_9YWw35-FO5DDw/edit?usp=sharing

# TP1
Forkear este proyecto y asignar al user tdd7510 como administrador.
## Enunciado
Se deberá implementar una herramienta que permita modelar una hoja de cálculo con las operaciones básicas detalladas a continuación. La interfaz e interacción con la misma será a modo de API desde la consola.
## Funcionalidad a Implementar
* Creación de una nueva planilla
* Agregar valores a las celdas
* Recuperar valor de una celta
* Agregar fórmulas a las celdas
* En esta iteración no se contemplara validación de referencias circulares en las fórmulas.
* Validar fórmulas (Si una fórmula no es válida y se pide el resultado de la celda que la utiliza directa o indirectamente, se retornará un mensaje de Fórmula Inválida)
* Agregar hojas adicionales a la planilla y poder referenciar fórmulas entre dichas hojas
* Soportar funciones de “Hacer” y “Deshacer”
* Fórmulas a soportar:

1. OPERACION SUMA

2. OPERACION RESTA

3. =OTRA_CELDA

4. =OTRA_CELDA2 + 4 ­ OTRA_CELDA3 + ....
## Objetivos
* Implementar una versión inicial de la herramienta con funcionalidad mínima.
* Aplicar las técnicas vistas en la teoría y en la práctica.
## Herramientas a utilizar
* Java >= 1.8
* Gradle >= 2.6
* JUnit >= 4.11
* Git ­> Bitbucket
* CheckStyle / PMD / Findbugs
## Restricciones
* Trabajo Práctico en grupos de 3 alumnos implementado en java.
* Se deben utilizar las mismas herramientas que en el TP0.
* Todas las clases del sistema deben estar justificadas.
* Todas las clases deben llevar un comentario con las responsabilidades de la misma.
* El uso de herencia debe estar justificado. Se debe explicar claramente el porqué de su conveniencia por sobre otras opciones.
* Se debe tener una cobertura completa del código por tests.
## Criterios de Corrección
* Cumplimiento de las restricciones
* Documentación entregada
* Diseño del modelo
* Diseño del código
* Test unitarios
* Se tendrán en cuenta también la completitud del tp, la correctitud, distribución de responsabilidades, aplicación y uso de criterios y principios de buen diseño, buen uso del repositorio y uso de buenas prácticas en general.