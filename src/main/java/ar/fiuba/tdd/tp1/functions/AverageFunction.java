package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.Parser;
import ar.fiuba.tdd.tp1.Spreadsheet;
import ar.fiuba.tdd.tp1.contents.DependencyHelper;

import java.util.List;

/**
 * Created by fran on 15/10/15.
 */
public class AverageFunction implements Function {

    @Override
    public String operate(String argument, DependencyHelper helper) throws IllegalArgumentException {
        List<Float> cells = helper.getCellValuesInRangeAsDouble(argument);
        float result = 0;
        for (Float cellValue : cells) {
            result += cellValue;
        }
        return Float.toString(result / cells.size());
    }
}
