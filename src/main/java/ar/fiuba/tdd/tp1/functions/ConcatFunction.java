package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.Spreadsheet;
import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.contents.DependencyHelper;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by fran on 16/10/15.
 */
public class ConcatFunction implements Function {

    private static final Pattern concatPattern = Pattern.compile("^[^(),]+(,[^(),]+)+");

    @Override
    public String operate(String argument, DependencyHelper helper) throws IllegalConcatArgumentException {
        if (!validate(argument)) {
            throw new IllegalConcatArgumentException();
        }
        String[] arguments = argument.split(",");
        StringBuffer result = new StringBuffer();
        for (String oneArgument : arguments) {
            result.append(concat(oneArgument,helper));
        }
        return result.toString();
    }

    private String concat(String argument,DependencyHelper helper) {
        StringBuffer buf = new StringBuffer();
        try {
            buf.append(helper.getValueAsString(argument));
        } catch (BadReferenceException badReferenceException) {
            buf.append(argument);
        }
        return buf.toString();
    }

    private boolean validate(String argument) {
        Matcher matcher = concatPattern.matcher(argument);
        return matcher.matches();
    }
}
