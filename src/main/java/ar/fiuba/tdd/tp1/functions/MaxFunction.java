package ar.fiuba.tdd.tp1.functions;

import ar.fiuba.tdd.tp1.Parser;
import ar.fiuba.tdd.tp1.Spreadsheet;
import ar.fiuba.tdd.tp1.contents.DependencyHelper;

import java.util.List;

/**
 * Created by fran on 15/10/15.
 */
public class MaxFunction implements Function {

    public String operate(String argument, DependencyHelper helper) {
        List<Float> cells = helper.getCellValuesInRangeAsDouble(argument);
        float max = cells.get(0);
        for (Float cellValue : cells) {
            if (cellValue > max) {
                max = cellValue;
            }
        }
        return Float.toString(max);
    }
}
