package ar.fiuba.tdd.tp1.formatter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Created by pc on 27/10/15.
 */
public class DateFormatter implements IFormatter {

    private DateTimeFormatter formatDate;
    @Expose @SerializedName("Date.Format")
    String dateFormat;

    public DateFormatter(String format) {
        dateFormat = format;

        format = format.replaceAll("Y", "y");
        format = format.replaceAll("D", "d");
        this.formatDate = DateTimeFormatter.ofPattern(format);
    }

    @Override
    public String format(String string) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

            LocalDate date = LocalDate.parse(string , formatter);
            return date.format(formatDate);
        } catch (DateTimeParseException error) {
            return "Error:BAD_DATE";
        }
    }

    @Override
    public String getName() {
        return "Date";
    }

    @Override
    public String getFormatValue() {
        return dateFormat;
    }
}
