package ar.fiuba.tdd.tp1;


import com.google.gson.*;

import ar.fiuba.tdd.tp1.contents.IContent;

import java.lang.reflect.Type;


/**
 * Created by pc on 17/10/15.
 */
public class IContentAdapter implements JsonSerializer<IContent>,JsonDeserializer<IContent> {

    private static final String CLASSNAME = "CLASSNAME";
    private static final String INSTANCE  = "INSTANCE";

    @Override
    public IContent deserialize(JsonElement element,Type type,JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = element.getAsJsonObject();
        JsonPrimitive prim = (JsonPrimitive) jsonObject.get(CLASSNAME);
        String className = prim.getAsString();

        Class<?> klass = null;

        try {
            klass = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new JsonParseException(e.getMessage());
        }
        return context.deserialize(jsonObject.get(INSTANCE), klass);
    }

    @Override
    public JsonElement serialize(IContent content, Type type, JsonSerializationContext jsonSerializationContext) {

        JsonObject retValue = new JsonObject();
        String className = content.getClass().getCanonicalName();
        retValue.addProperty(CLASSNAME, className);
        JsonElement elem = jsonSerializationContext.serialize(content.getRaw());
        retValue.add(INSTANCE, elem);
        return retValue;
    }
}