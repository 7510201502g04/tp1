package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriver;

import java.util.Scanner;

public class API {
    static SpreadSheetDriver driver;
    static String currentSpreadsheet;

    static String commandSeparator = ">";
    static String argumentSeparator = "_";

    public static void main(String[] args) {
        driver = new SpreadSheetDriver();
        Scanner scan = new Scanner(System.in, "UTF-8");
        String command;
        printHelp();
        driver.createNewWorkBookNamed("default");
        currentSpreadsheet = "default";
        while (true) {
            command = scan.nextLine();
            execute(command);
        }

    }

    private static void execute(String command) {
        command = command.replaceAll(" ", "");
        if (checkSpreadsheetsCommands(command)) {
            return;
        } else {
            if (checkSheetsCommands(command)) {
                return;
            } else {
                if (checkCellCommands(command)) {
                    return;
                } else {
                    if (checkSaveCommands(command)) {
                        return;
                    }
                }
            }
        }
        System.out.println("Invalid command");
    }

    private static boolean checkSaveCommands(String command) {
        String newCommand = command.split(commandSeparator)[0];
        switch (newCommand) {
            case "csv":
                saveCsv(command); break;
            case "json":
                saveJson(command); break;
            case "importJson":
                importFromJsonFile(command);
                break;
            case "importCSV":
                importFromCSVFile(command);
                break;
            default:
                return false;
        }
        return true;
    }

    private static boolean checkSheetsCommands(String command) {
        String newCommand = command.split(commandSeparator)[0];
        switch (newCommand) {
            case "lss":
                listSheets(); break;
            case "undo":
                undo(); break;
            case "as":
                addSheet(command); break;
            case "redo":
                redo(); break;
            default:
                return false;
        }
        return true;
    }

    private static boolean checkSpreadsheetsCommands(String command) {
        String newCommand = command.split(commandSeparator)[0];
        switch (newCommand) {
            case "css":
                createSpreadsheet(command);
                break;
            case "chss":
                changeCurrentSpreadSheet(command);
                break;
            case "ls":
                listSpreadSheets();
                break;
            case "chcs":
                changeCurrentSheet(command);
                break;
            default:
                return false;
        }
        return true;
    }


    private static void saveCsv(String command) {
        String file = command.split(commandSeparator)[1];
        driver.saveCSV(currentSpreadsheet, file);
    }

    private static boolean checkCellCommands(String command) {
        String newCommand = command.split(commandSeparator)[0];
        switch (newCommand) {
            case "print":
                printSheet();
                break;
            case "cv":
                cellValue(command);
                break;
            case "scc":
                setCellContent(command);
                break;
            case "help":
                printHelp();
                break;
            default:
                return false;
        }
        return true;
    }

    private static void printSheet() {
        driver.printSheet(currentSpreadsheet);
    }

    private static void importFromJsonFile(String command) {
        String fileName = command.split(commandSeparator)[1];
        driver.reloadPersistedWorkBook(fileName);
    }


    private static void saveJson(String command) {
        String file = command.split(commandSeparator)[1];
        driver.saveJson(currentSpreadsheet, file);
    }

    private static void createSpreadsheet(String command) {
        String[] temp = command.split(commandSeparator);
        if (temp.length != 2) {
            System.out.println("Invalid command");
            return;
        }
        driver.createNewWorkBookNamed(temp[1]);
        currentSpreadsheet = temp[1];
        System.out.println("A new spreadsheet with a sheet has been created");
    }

    private static void importFromCSVFile(String command) {
        String [] commands = command.split(commandSeparator);
        String[] parameters = commands[1].split(argumentSeparator);
        driver.loadFromCSV(currentSpreadsheet, parameters[1], parameters[0]);
    }

    private static void listSpreadSheets() {
        driver.printSpreadsheets();
    }

    private static void changeCurrentSpreadSheet(String command) {
        String[] temp = command.split(commandSeparator);
        try {
            driver.validateSpreadsheetName(temp[1]);
            currentSpreadsheet = temp[1];
            System.out.println("Changed current spread sheet to " + currentSpreadsheet);
        } catch (Exception e) {
            System.out.println("Invalid spreadsheet name");
        }
    }

    private static void addSheet(String command) {
        String[] temp = command.split(commandSeparator);
        driver.createNewWorkSheetNamed(currentSpreadsheet,temp[1]);
        System.out.println("A new sheet has been added");
    }

    private static void changeCurrentSheet(String command) {
        String[] temp = command.split(">");
        String name = temp[1];
        try {
            driver.setCurrentSheet(currentSpreadsheet,name);
            System.out.println("Changed current sheet to " + name);
        } catch (Exception e) {
            System.out.println("Invalid argument");
        }
    }

    private static void listSheets() {
        driver.printSheets(currentSpreadsheet);
    }



    private static void cellValue(String command) {
        String[] temp = command.split(commandSeparator);
        String cellPosition = temp[1];
        try {
            String cellValue = driver.getStringCellValue(currentSpreadsheet, cellPosition);
            System.out.println("Value of cell " + cellPosition + " is " + cellValue);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void setCellContent(String command) {
        try {
            String cellPositionAndValue = command.split(commandSeparator)[1];
            String cellPosition = cellPositionAndValue.split(argumentSeparator)[0];
            String cellValue = cellPositionAndValue.split(argumentSeparator)[1];
            driver.setValueInCell(currentSpreadsheet, cellPosition, cellValue);
        } catch (Exception e) {
            System.out.println("Formato invalido");
        }
    }

    private static void undo() {
        driver.undo();
        System.out.println("An action has been undone");
    }

    private static void redo() {
        driver.redo();
        System.out.println("An action has been redone");
    }


    private static void printHelp() {
        printHelpOne();
        printHelpTwo();
    }

    private static void printHelpOne() {
        System.out.println("The syntax for a cell position is a letter followed by a number (followed by S[sheetNumber]"
                + " if you want to reference another sheet)."
                + "For example A4 or F3S2. To put a formula start with an '=' sign. For example, 'scc>A4_A2+2");
        System.out.println("Enter one of the following options: ");
        System.out.println("css" + commandSeparator + "[SpreadsheetName]                       Creates a new spreadSheet");
        System.out.println("ls                                          Lists current spreadsheets");
        System.out.println("chcss" + commandSeparator + "[name]                                Changes current spreadsheet number");
        System.out.println("as" + commandSeparator + "[name]                                   Adds a new sheet");
        System.out.print("lss                                         Lists current number of sheets");
        System.out.println();
        System.out.println("cv" + commandSeparator + "[cellPosition]                           Get the value of the cell");
        System.out.println("scc" + commandSeparator + "[cellPosition]" + argumentSeparator + "[cellValue]"
                +  "              Set the value of the cell");
    }

    private static void printHelpTwo() {
        System.out.println("print                                       Print current sheet of current spreadsheet");
        System.out.println("undo                                        Undo last action");
        System.out.println("redo                                        Redoes last action");
        System.out.println("csv" + commandSeparator + "[file]                                  Saves current sheet as csv in file");
        System.out.print("json" + commandSeparator + "[file]                                 Saves current spreadsheet as json in file");
        System.out.println();
        System.out.println("redo                                        Redoes last action");
        System.out.println("importJson" + commandSeparator + "[file]                           Reload from Json");
        System.out.println("chcs" + commandSeparator + "[name]                                 Change current sheet");
        System.out.println("importCSV" + commandSeparator + "[file]" + argumentSeparator + "[sheet]"
                + "                    Load a sheet from a csv file");
        System.out.println("help                                        Displays this message");
    }
}