package ar.fiuba.tdd.tp1.acceptance.driver;

public class BadReferenceException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private final boolean cycleProblem;

    public BadReferenceException(boolean cycleProblem) {
        this.cycleProblem = cycleProblem;
    }

    public boolean isCycleProblem() {
        return cycleProblem;
    }
}
