package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.Spreadsheet;

/**
 * Created by ezequiel on 17/10/15.
 */
public class GetRawCommand implements ReversibleCommand {

    private final Spreadsheet spreadsheet;
    private final String cell;

    public GetRawCommand(Spreadsheet spreadsheet , String cell) {
        this.cell = cell;
        this.spreadsheet = spreadsheet;
    }

    @Override
    public String execute() {
        return spreadsheet.getCurrentSheet().getCell(cell).getRaw();

    }

    @Override
    public void undo() {

    }

    @Override
    public void redo() {

    }
}
