package ar.fiuba.tdd.tp1.contents;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by pc on 22/10/15.
 */
public class DateContent implements IContent {

    SimpleDateFormat formatter;
    private Date date;

    public DateContent(String format,String oneDate) throws ParseException {
        formatter = new SimpleDateFormat(format);
        date = formatter.parse(oneDate);
    }

    @Override
    public String getValue() {
        return formatter.format(date);
    }

    @Override
    public String getRaw() {
        return formatter.format(date);
    }

    public void changeFormat(String format) throws ParseException {
        formatter = new SimpleDateFormat(format);
    }
}
