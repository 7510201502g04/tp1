package ar.fiuba.tdd.tp1.contents;

import ar.fiuba.tdd.tp1.FloatUtil;
import ar.fiuba.tdd.tp1.NotFormulaException;
import ar.fiuba.tdd.tp1.acceptance.driver.BadFormulaException;
import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.functions.*;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ezequiel on 28/09/15.
 */
public class FormulaContent implements IContent {
    private final DependencyHelper dependencyHelper;
    String formula;

    private static final Pattern formulaPattern = Pattern.compile("[^,(+\\-=]*\\([^()]*\\)");
    private static final Pattern rangePattern = Pattern.compile("^(!\\w+\\.)?[A-z][0-9]+:[A-z][0-9]+$");
    private HashMap<String,Function> formulas;

    private static final String average = "AVERAGE";
    private static final String min = "MIN";
    private static final String max = "MAX";
    private static final String concat = "CONCAT";
    private String actualCellsaver;


    public FormulaContent(String newFormula, DependencyHelper dependencyHelper) {
        this.formula = newFormula;
        this.formulas = new HashMap<>();
        this.formulas.put(average, new AverageFunction());
        this.formulas.put(min, new MinFunction());
        this.formulas.put(max, new MaxFunction());
        this.formulas.put(concat, new ConcatFunction());
        this.dependencyHelper = dependencyHelper;

    }

    @Override
    public String getValue() throws IllegalArgumentException {
        return resolveFormula(formula);
    }

    @Override
    public String getRaw() {
        return formula;
    }


    private String resolveFormula(String formula) throws NotFormulaException {
        if (!formula.startsWith("=")) {
            throw new NotFormulaException();
        }
        formula = formula.substring(1, formula.length());
        formula = formula.replace(" ", "");
        return checkFormulas(formula);
    }

    private String checkFormulas(String formula) {
        boolean found = true;
        boolean isString = false;
        String value = "";
        while (found) {
            found = false;
            Matcher matcher = formulaPattern.matcher(formula);
            while (matcher.find()) {
                found = true;
                value = resolveFunction( matcher.group(0));
                formula = formula.replace(matcher.group(0), value);
                isString = isString(value);
            }
        }
        return solveCheckedFormula(value, isString, formula);
    }

    private boolean isString(String value) {
        boolean isString = false;
        try {
            Float.parseFloat(value);
        } catch (Exception e) {
            isString = true;
        }
        return isString;
    }

    private String resolveFunction(String possibleFormula) {
        String formulaName = possibleFormula.replaceFirst("\\([^()]*\\)", "");
        if (this.formulas.containsKey(formulaName)) {
            String argument = possibleFormula.substring(formulaName.length() + 1, possibleFormula.length() - 1);
            return this.formulas.get(formulaName).operate(argument, dependencyHelper);
        } else {
            throw new IllegalArgumentException();
        }
    }


    private String solveCheckedFormula(String value, boolean isString,String  formula) {
        if (isString) {
            if (formula.compareTo(value) != 0) {
                throw new IllegalArgumentException();
            }
            return formula;
        }
        return FloatUtil.toString(parseSum(formula, new NumericContent(0)));
    }


    private float parseSum(String formula, NumericContent index) {
        float result = this.parseTerm(formula, index);
        int iterator = (int) index.getNumericValue();
        try {
            while (iterator < formula.length()) {
                if (formula.charAt(iterator) == '+') {
                    index.setValue(iterator + 1);
                    result += this.parseTerm(formula, index);
                    iterator = (int) index.getNumericValue();
                } else {
                    index.setValue(iterator + 1);
                    result -= this.parseTerm(formula, index);
                    iterator = (int) index.getNumericValue();
                }
            }
        } catch (Exception e) {
            if ( e  instanceof BadReferenceException && ((BadReferenceException) e).isCycleProblem()) {
                throw e;
            }
            throw new BadFormulaException();
        }
        return result;
    }

    private float parseTerm(String formula, NumericContent index) {
        int iterator = (int) index.getNumericValue();
        if (formula.charAt(iterator) == '-') {
            index.setValue(++iterator);
            return -parseSum(formula,index);
        }
        StringBuffer buf = new StringBuffer();
        actualCellsaver = DependencyHelper.getActualCell();
        do {
            if (formula.charAt(iterator) == '+' || formula.charAt(iterator) == '-') {
                index.setValue(iterator);

                return this.validate(buf.toString());
            }
            buf.append(formula.charAt(iterator));
            iterator++;
        } while (iterator < formula.length());
        index.setValue(iterator);
        return this.validate(buf.toString());
    }

    private float validate(String term) {
        try {
            return Float.parseFloat(term);
        } catch (IllegalArgumentException e) {
            if (term.isEmpty()) {
                return 0;
            }
            float value = dependencyHelper.getValueFromCell(term);
            DependencyHelper.setActualCell(actualCellsaver);
            return value;
        }
    }
}
