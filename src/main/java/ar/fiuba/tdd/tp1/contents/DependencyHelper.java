package ar.fiuba.tdd.tp1.contents;

import ar.fiuba.tdd.tp1.NotFormulaException;
import ar.fiuba.tdd.tp1.Parser;
import ar.fiuba.tdd.tp1.Sheet;
import ar.fiuba.tdd.tp1.Spreadsheet;
import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ezequiel on 25/10/15.
 */
public class DependencyHelper {

    private static boolean cycleFound;
    private static DefaultDirectedGraph<String, DefaultEdge> graph;

    private static Spreadsheet spreadsheet;
    private static Sheet sheet;
    private static String actualCell;

    public DependencyHelper(Spreadsheet spreadsheet, Sheet sheet, String cell) {
        setSpreadsheet(spreadsheet);
        setSheet(sheet);
        setActualCell(cell);
        setGraph(null);
    }


    public static String getActualCell() {
        return actualCell;
    }

    public static void setSpreadsheet(Spreadsheet spreadsheet) {
        DependencyHelper.spreadsheet = spreadsheet;
    }

    public static void setSheet(Sheet sheet) {
        DependencyHelper.sheet = sheet;
    }

    public static void setGraph(DefaultDirectedGraph<String, DefaultEdge> graph) {
        DependencyHelper.graph = graph;
    }

    public List<Float> getCellValuesInRangeAsDouble(String range) {
        List<String> cellValuesAsString = getCellValuesInRange(range);
        List<Float> cellValues = new LinkedList<>();
        for (String string : cellValuesAsString) {
            try {
                cellValues.add(Float.parseFloat(string));
            } catch (Exception e) {
                //Ignored
            }
        }
        return cellValues;
    }

    public List<String> getCellValuesInRange(String range) throws IllegalArgumentException {
        if (!Parser.validateRange(range)) {
            throw new IllegalArgumentException("Invalid range");
        }
        Sheet sheet = DependencyHelper.sheet;
        if (range.startsWith("!")) {
            String sheetName = range.replaceAll("\\.[A-z][0-9]+:[A-z][0-9]+$","").replaceAll("^!","");
            Sheet sheetAux = spreadsheet.getSheet(sheetName);
            if (sheetAux == null) {
                throw new IllegalArgumentException("Invalid sheet name");
            }
            sheet = sheetAux;
            range = range.replaceAll("^(!\\w+\\.)","");
        }

        return cellValues(range, sheet);
    }

    private List<String> cellValues(String range, Sheet sheet) {
        String[] rangeCells = range.split(":");
        String firstCell = rangeCells[0];
        char firstColumn = firstCell.charAt(0);
        int firstLine = Integer.parseInt(firstCell.substring(1));
        String secondCell = rangeCells[1];
        char secondColumn = secondCell.charAt(0);
        int secondLine = Integer.parseInt(secondCell.substring(1));
        if (secondColumn < firstColumn) {
            char aux = secondColumn;
            secondColumn = firstColumn;
            firstColumn = aux;
        }
        if (secondLine < firstLine) {
            int aux = secondLine;
            secondLine = firstLine;
            firstLine = aux;
        }
        return makeList(firstLine, secondLine, firstColumn, secondColumn, sheet);
    }


    private List<String> makeList(int firstLine, int secondLine, char firstColumn, char secondColumn,Sheet sheet) {
        String saveActualCell = actualCell;
        List<String> cells = new LinkedList<>();
        for (char i = firstColumn;i <= secondColumn;i++) {
            for (int j = firstLine;j <= secondLine;j++) {
                setActualCell(saveActualCell);
                String cellPosition = i + Integer.toString(j);
                actualCellDependsOn(cellPosition);
                setActualCell(cellPosition);
                try {
                    String cellValue = sheet.getCell(cellPosition).getContent().getValue();
                    cells.add(cellValue);
                } catch (NotFormulaException e) {
                    cells.add(sheet.getCell(cellPosition).getContent().getRaw());
                } catch (Exception e) {
                    //Ignored
                }

            }
        }
        return cells;
    }

    private static final String otherSheetCellPattern = "^(!\\w+\\.)[A-z][0-9]+$";

    public float getValueFromCell(String argument) {
        actualCellDependsOn(argument);
        setActualCell(argument);
        return spreadsheet.getValueFromCell(argument);
    }

    public String getValueAsString(String argument) {
        actualCellDependsOn(argument);
        setActualCell(argument);
        return spreadsheet.getValueAsString(sheet.getName(), argument);
    }

    public static void startCycleController(String cell) {
        cycleFound = false;
        graph = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
        graph.addVertex(cell);
        actualCell = cell;
    }

    public static boolean cycleFound() {
        return cycleFound;
    }

    public void actualCellDependsOn(String term) throws BadReferenceException {
        if (graph == null) {
            return;
        }
        graph.addVertex(term);
        if (actualCell != null) {
            graph.addEdge(actualCell, term);
        }
        checkCycles();
    }

    private void checkCycles() {
        CycleDetector cycleDetector = new CycleDetector<String, DefaultEdge>(graph);
        if (cycleDetector.detectCycles()) {
            cycleWasFound();
            throw new BadReferenceException(true);
        }
    }

    public static void setActualCell(String cell) {
        actualCell = cell;
    }

    public static void cycleWasFound() {
        cycleFound = true;
    }

}
