package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.acceptance.driver.BadFormatException;
import ar.fiuba.tdd.tp1.contents.EmptyContent;
import ar.fiuba.tdd.tp1.contents.IContent;
import ar.fiuba.tdd.tp1.contents.StringContent;
import ar.fiuba.tdd.tp1.formatter.IFormatter;

/**
 * Created by ezequiel on 27/09/15.
 */
public class Cell implements Comparable<Cell> {
    String sheet;
    String id;
    IContent value = new EmptyContent();
    IFormatter formatter = null;
    String type;

    public Cell(String sheet, String id) {
        this.sheet = sheet;
        this.id = id;
        this.type = "";

    }

    public void setCellType(String type) {
        this.type = type;
    }

    public void setFormatter(IFormatter formatter) {

        this.formatter = formatter;
    }

    public String getType() {
        return this.type;
    }

    public IFormatter getFormatter() {
        return this.formatter;
    }

    public String getID() {
        return this.id;
    }

    public IContent getContent() {
        return value;
    }

    public void setNewContent(IContent newContent) {
        value = newContent;
    }

    public String getRaw() {

        if (this.formatter == null) {
            return getContent().getValue();
        }
        return formatter.format(getContent().getValue());
    }

    public String getSheet() {
        return this.sheet;
    }

    public char getColumn() {
        return this.getID().charAt(0);
    }

    public int getRow() {
        return Integer.parseInt(id.substring(1));
    }

    @Override
    public int compareTo(Cell other) {
        if (this.getRow() < other.getRow()) {
            return -1;
        } else if (this.getRow() > other.getRow()) {
            return 1;
        } else {
            if (this.getColumn() < other.getColumn()) {
                return -1;
            }
        }
        return 1;
    }

    /*
    Metodos equals y hashcode inventado para que findbugs no moleste
     */

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Cell)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return 31 * id.hashCode() + type.hashCode();
    }

    public String getValue() {
        if (this.type.equals("String")) {
            String value = getContent().getRaw();
            try {
                Float.parseFloat(value);
            } catch (Exception e) {
                return value;
            }
            throw new BadFormatException();
        }
        return getRaw();
    }
}
