package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.commands.AddSheetCommand;
import ar.fiuba.tdd.tp1.commands.GetValueCommand;
import ar.fiuba.tdd.tp1.commands.SetValueCommand;
import ar.fiuba.tdd.tp1.contents.FormulaContent;
import ar.fiuba.tdd.tp1.contents.NumericContent;
import ar.fiuba.tdd.tp1.contents.StringContent;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by pc on 20/10/15.
 */
public class CsvHandlerTest {
    private static final double DELTA = 0.00001;
    Spreadsheet spreadsheet = new Spreadsheet("");
    private CommandManager commandManager;

    public void setUp() {

        commandManager = new CommandManager();
        AddSheetCommand addSheetCommand = new AddSheetCommand(spreadsheet,"Prueba");
        commandManager.executeCommand(addSheetCommand);

        commandManager.executeCommand(new SetValueCommand(spreadsheet,"default","A1","A1"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"Prueba","A2","A2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"Prueba","A3","A3"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"Prueba","A5","A5"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"Prueba","B1", "B1"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"Prueba","B2","B2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"Prueba","D5","D5"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"Prueba","D1","D1"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"Prueba","F6","F6"));
    }

    @Test
    public void csvWrB1iteandReadSheet() throws IOException {
        this.setUp();
        CsvHandler csv = new CsvHandler("sheet");
        csv.write(spreadsheet);

        Spreadsheet spreadsheet1 = csv.read("sheet");
        commandManager = new CommandManager();

        assertEquals("A2",commandManager.executeCommand(new GetValueCommand(spreadsheet1, "A2")));
        assertEquals("A3",commandManager.executeCommand(new GetValueCommand(spreadsheet1, "A3")));
        assertEquals("A5",commandManager.executeCommand(new GetValueCommand(spreadsheet1, "A5")));
        assertEquals("B2",commandManager.executeCommand(new GetValueCommand(spreadsheet1, "B2")));


    }
}
