package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.functions.IllegalConcatArgumentException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by fran on 15/10/15.
 */
public class FunctionsTest {
    private static final double DELTA = 0.00001;
    private Spreadsheet spreadsheet;
    private Sheet sheet;

    @Before
    public void setUp() {
        this.spreadsheet = new Spreadsheet("");
        this.sheet = spreadsheet.getCurrentSheet();
        this.spreadsheet.setCellValue("default","A1","5");
        this.spreadsheet.setCellValue("default","A2","3");
        this.spreadsheet.setCellValue("default","A3","=what");
        this.spreadsheet.setCellValue("default","B1","6");
        this.spreadsheet.setCellValue("default","B2","10");
        this.spreadsheet.setCellValue("default","B3","why");
        this.spreadsheet.setCellValue("default","C1","hello ");
        this.spreadsheet.setCellValue("default", "C2", "world");
        spreadsheet.addSheet(new Sheet("another"));
        this.spreadsheet.setCellValue("another","C2","number");
        spreadsheet.setCurrentSheet("default");
    }

    @Test
    public void averageFunction() {
        this.spreadsheet.setCellValue("default","B5","=AVERAGE(A1:B3)");
        assertEquals(6, this.spreadsheet.getValueFromCell("B5"), DELTA);
        this.spreadsheet.setCellValue("default","B5","=AVERAGE(B3:A1)");
        assertEquals(6, this.spreadsheet.getValueFromCell("B5"), DELTA);
        this.spreadsheet.setCellValue("default", "B1", "-2");
        assertEquals(4, this.spreadsheet.getValueFromCell("B5"), DELTA);
    }

    @Test
    public void averageFunctionInOtherSheet() {
        this.spreadsheet.addSheet(new Sheet("other"));
        this.spreadsheet.setCellValue("other","B5","=AVERAGE(!default.A1:B3)");
        assertEquals(6, this.spreadsheet.getValueFromCellInSheet("other", "B5"), DELTA);
        this.spreadsheet.setCellValue("default", "B1", "-2");
        assertEquals(4, this.spreadsheet.getValueFromCellInSheet("other", "B5"), DELTA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidRangeArgument() {
        this.spreadsheet.setCellValue("default","B5","=AVERAGE(!invalid.A1:B3)");
        this.spreadsheet.getValueAsString("default", "B5");
    }

    @Test
    public void minFunction() {
        this.spreadsheet.setCellValue("default", "B5", "=MIN(A1:B3)");
        assertEquals(3,Float.parseFloat(this.spreadsheet.getValueAsString("default", "B5")),DELTA);
        this.spreadsheet.setCellValue("default", "B1", "-2");
        assertEquals(-2, Float.parseFloat(this.spreadsheet.getValueAsString("default", "B5")), DELTA);
    }

    @Test
    public void maxFunction() {
        this.spreadsheet.setCellValue("default","B5","=MAX(A1:B2)");
        assertEquals(10,this.spreadsheet.getValueFromCell("B5"),DELTA);
        this.spreadsheet.setCellValue("default", "B1", "20");
        assertEquals(20, this.spreadsheet.getValueFromCell("B5"), DELTA);
    }

    @Test
    public void concatFunctionWithFloats() {
        this.spreadsheet.setCellValue("default","B5","=CONCAT(A1,B2)");
        assertEquals(510,this.spreadsheet.getValueFromCell("B5"),DELTA);
        this.spreadsheet.setCellValue("default", "B2", "20");
        assertEquals(520, this.spreadsheet.getValueFromCell("B5"), DELTA);
    }

    @Test
    public void concatFunctionWithStrings() {
        this.spreadsheet.setCellValue("default","B5","=CONCAT(C1,C2)");
        assertEquals("hello world",this.spreadsheet.getValueAsString("default","B5"));
        this.spreadsheet.setCellValue("default","B5","=CONCAT(C1,C2,A1)");
        assertEquals("hello world5",this.spreadsheet.getValueAsString("default", "B5"));
        this.spreadsheet.setCellValue("default","B5","=CONCAT(C1,C2,A1,A2)");
        assertEquals("hello world53",this.spreadsheet.getValueAsString("default", "B5"));
        this.spreadsheet.setCellValue("default","B5","=CONCAT(C1,C2,!another.C2)");
        assertEquals("hello worldnumber",this.spreadsheet.getValueAsString("default", "B5"));
    }

    @Test
    public void concatFunctionWithConcat() {
        this.spreadsheet.setCellValue("default","B5","=CONCAT(CONCAT(C1,C2),C2)");
        assertEquals("hello worldworld",this.spreadsheet.getValueAsString("default","B5"));
        this.spreadsheet.setCellValue("default","B5","=CONCAT(CONCAT(C1,C2),CONCAT(C1,C2))");
        assertEquals("hello worldhello world",this.spreadsheet.getValueAsString("default","B5"));
    }

    @Test(expected = IllegalConcatArgumentException.class)
    public void invalidConcatArgument() {
        this.spreadsheet.setCellValue("default","B5","=CONCAT(C1.C2)");
        this.spreadsheet.getValueAsString("default", "B5");
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidFormulaArgument() {
        this.spreadsheet.setCellValue("default","B5","=CONCAT(C1,C2) + 5");
        this.spreadsheet.getValueAsString("default", "B5");
    }
}
