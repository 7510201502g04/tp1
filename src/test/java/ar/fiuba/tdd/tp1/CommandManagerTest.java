package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.commands.AddSheetCommand;
import ar.fiuba.tdd.tp1.commands.GetRawCommand;
import ar.fiuba.tdd.tp1.commands.GetValueCommand;
import ar.fiuba.tdd.tp1.commands.SetValueCommand;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CommandManagerTest {

    private static final double DELTA = 0.00001;
    private Spreadsheet spreadsheet;
    private CommandManager commandManager;

    @Before
    public void setUp() {
        spreadsheet = new Spreadsheet("");
        commandManager = new CommandManager();
    }

    @Test
    public void addingNewSheetToSpreadsheet() {
        List<String> names;
        commandManager.executeCommand(new AddSheetCommand(spreadsheet, "newSheet"));
        names = spreadsheet.getNames();
        assertTrue(names.contains("newSheet"));
        commandManager.undo();
        names = spreadsheet.getNames();
        assertFalse(names.contains("newSheet"));
        commandManager.redo();
        names = spreadsheet.getNames();
        assertTrue(names.contains("newSheet"));
    }

    @Test
    public void reversingTwoActions() {
        commandManager.executeCommand(new AddSheetCommand(spreadsheet, "zarasa"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, "zarasa", "A1", "12"));
        commandManager.redo();
        assertEquals("zarasa", spreadsheet.getCurrentSheet().getName());
        GetValueCommand getValueCommand = new GetValueCommand(spreadsheet,"A1");
        String value = commandManager.executeCommand(getValueCommand);
        assertEquals(12, Float.parseFloat(value), DELTA);
        commandManager.undo();
        assertTrue(spreadsheet.getNames().contains("zarasa"));
        assertEquals("", commandManager.executeCommand(getValueCommand));
        commandManager.undo();
        assertFalse(spreadsheet.getNames().contains("zarasa"));
        commandManager.undo();
    }

    @Test
    public void settingNumericValueInACell() {
        Float value;
        String name = spreadsheet.getCurrentSheet().getName();
        assertEquals("", spreadsheet.getValueAsString(name, "A1"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "A1", "12"));
        value = spreadsheet.getValueFromCellInSheet(name, "A1");
        assertEquals(12, value, DELTA);
        commandManager.undo();
        assertEquals("", spreadsheet.getValueAsString(name, "A1"));
        commandManager.redo();
        value = spreadsheet.getValueFromCellInSheet(name, "A1");
        assertEquals(12, value, DELTA);
    }

    @Test
    public void getValueCommand() {
        GetValueCommand getValueCommand = new GetValueCommand(spreadsheet,"A5");
        assertEquals("", commandManager.executeCommand(getValueCommand));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, spreadsheet.getCurrentSheet().getName(), "A5", "10"));
        assertEquals(10, Float.parseFloat(commandManager.executeCommand(getValueCommand)), DELTA);
        commandManager.undo();
        commandManager.undo();
        commandManager.undo();
        assertEquals("", commandManager.executeCommand(getValueCommand));
    }

    @Test
    public void settingStringValueInACell() {
        String name = spreadsheet.getCurrentSheet().getName();
        assertEquals("", spreadsheet.getValueAsString(name, "A1"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "A1", "ZARASA"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,name,"A1", "LALALA"));
        assertEquals("LALALA", commandManager.executeCommand(new GetValueCommand(spreadsheet, "A1")));
        commandManager.undo();
        assertEquals("ZARASA", commandManager.executeCommand(new GetValueCommand(spreadsheet, "A1")));
        commandManager.undo();
        assertEquals("", commandManager.executeCommand(new GetValueCommand(spreadsheet, "A1")));
        commandManager.redo();
        assertEquals("ZARASA", commandManager.executeCommand(new GetValueCommand(spreadsheet, "A1")));
        commandManager.redo();
        assertEquals("LALALA", commandManager.executeCommand(new GetValueCommand(spreadsheet, "A1")));
    }
    
    @Test
    public void referencingSumValueInACell() {
        String name = spreadsheet.getCurrentSheet().getName();
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "A1", "123"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "A2", "=A1 + 5"));
        assertEquals("128", commandManager.executeCommand(new GetValueCommand(spreadsheet, "A2")));
    }

    @Test
    public void referencingNumericGetRawInACell() {
        String name = spreadsheet.getCurrentSheet().getName();
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "A1", "-123"));
        assertEquals("-123", commandManager.executeCommand(new GetRawCommand(spreadsheet, "A1")));  //TODO: deveria devolver -123
    }


    @Test(expected = BadReferenceException.class)
    public void cycles() {
        String name = spreadsheet.getCurrentSheet().getName();
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "A1", "=2 + A2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "A2", "=A1 + 3"));
        commandManager.executeCommand(new GetValueCommand(spreadsheet, "A1"));
    }

    @Test(expected = BadReferenceException.class)
    public void otherCycles() {
        String name = spreadsheet.getCurrentSheet().getName();
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "A1", "=2 + B3"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "C2", "=A1 + 3"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "B3", "=C2 + 3"));
        commandManager.executeCommand(new GetValueCommand(spreadsheet, "A1"));
    }


    @Test(expected = BadReferenceException.class)
    public void cycleInAverage() {
        String name = spreadsheet.getCurrentSheet().getName();
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "B1", "2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "B2", "2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "B5", "=AVERAGE(B1:B5)"));
        commandManager.executeCommand(new GetValueCommand(spreadsheet, "B5"));
    }


    @Test(expected = BadReferenceException.class)
    public void cycleInMax() {
        String name = spreadsheet.getCurrentSheet().getName();
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "B1", "2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "B2", "2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "B5", "=MAX(B1:B5)"));
        commandManager.executeCommand(new GetValueCommand(spreadsheet, "B5"));
    }

    @Test(expected = BadReferenceException.class)
    public void cycleNotInGettingCell() {
        String name = spreadsheet.getCurrentSheet().getName();
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "B1", "=B2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "B2", "=B1"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, name, "B5", "=MIN(B1:B2)"));
        commandManager.executeCommand(new GetValueCommand(spreadsheet, "B5"));
    }

    @Test
    public void cycleInSum() {
        CommandManager commandManager = new CommandManager();
        AddSheetCommand addSheetCommand = new AddSheetCommand(spreadsheet,"Prueba");
        commandManager.executeCommand(addSheetCommand);
        SetValueCommand svc = new SetValueCommand(spreadsheet,"default","A1","5"); //5
        commandManager.executeCommand(svc);
        SetValueCommand svc1 = new SetValueCommand(spreadsheet,"Prueba","A2","2"); //2
        commandManager.executeCommand(svc1);
        SetValueCommand svc2 = new SetValueCommand(spreadsheet,"Prueba","A3","=A2"); // 2
        commandManager.executeCommand(svc2);
        SetValueCommand svc3 = new SetValueCommand(spreadsheet,"Prueba","A5","=A2 + 5"); //7
        commandManager.executeCommand(svc3);
        SetValueCommand svc4 = new SetValueCommand(spreadsheet,"Prueba","B1", "10"); //10
        commandManager.executeCommand(svc4);
        SetValueCommand svc5 = new SetValueCommand(spreadsheet,"Prueba","B2","=B1 + A3 - A5");// 5
        commandManager.executeCommand(svc5);
        String value = commandManager.executeCommand(new GetValueCommand(spreadsheet, "B2"));
        assertEquals("5",value);

    }

    @Test(expected = BadReferenceException.class)
    public void cyclesBetweenSheets() {
        CommandManager commandManager = new CommandManager();
        AddSheetCommand addSheetCommand = new AddSheetCommand(spreadsheet,"Prueba");
        commandManager.executeCommand(addSheetCommand);
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"default","A1","=A2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"default","A2","=!Prueba.B2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"Prueba","B2","=B3 + 3"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, "Prueba", "B3", "=B2 + 5"));
        spreadsheet.setCurrentSheet("default");
        String value = commandManager.executeCommand(new GetValueCommand(spreadsheet, "A1"));
    }


    @Test
    public void formulaInOtherSpreadsheet() {
        CommandManager commandManager = new CommandManager();
        AddSheetCommand addSheetCommand = new AddSheetCommand(spreadsheet,"Prueba");
        commandManager.executeCommand(addSheetCommand);
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"default","A1","=A2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"default","A2","=!Prueba.B2"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet,"Prueba","B2","=B3 + 3"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, "Prueba", "B3", "5"));
        commandManager.executeCommand(new SetValueCommand(spreadsheet, "default", "B3", "=100"));
        spreadsheet.setCurrentSheet("default");
        String value = commandManager.executeCommand(new GetValueCommand(spreadsheet, "A1"));
        assertEquals("8", value);
    }
}
