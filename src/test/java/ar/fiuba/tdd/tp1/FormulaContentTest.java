package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.contents.DependencyHelper;
import ar.fiuba.tdd.tp1.contents.FormulaContent;
import ar.fiuba.tdd.tp1.contents.NumericContent;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by pc on 27/09/15.
 */
public class FormulaContentTest {
    private static final double DELTA = 0.00001;
    private Spreadsheet spreadsheet;
    private Sheet sheet;

    @Before
    public void setUp() {
        this.spreadsheet = new Spreadsheet("");
        this.sheet = spreadsheet.getCurrentSheet();
    }

    @Test
    public void cellReferencingOtherCell() {
        Cell cellA1 = sheet.getCell("A1");
        Cell cellB2 = sheet.getCell("B2");
        cellB2.setNewContent(new FormulaContent("=A1", new DependencyHelper(spreadsheet, sheet, "B2")));
        assertEquals(0, spreadsheet.getValueFromCellInSheet("default","B2"),DELTA);
        cellA1.setNewContent(new NumericContent(5));
        assertEquals(5, spreadsheet.getValueFromCell("B2"), DELTA);
    }

    @Test
    public void formulaWithNumbers() {
        Cell cellA1 = sheet.getCell("A1");
        cellA1.setNewContent(new FormulaContent("=5 + 6 - 4", new DependencyHelper(spreadsheet, sheet, "A1")));
        assertEquals(7, spreadsheet.getValueFromCell("A1"), DELTA);
        cellA1.setNewContent(new FormulaContent("=+5 + 6 - 4",  new DependencyHelper(spreadsheet, sheet, "A1")));
        assertEquals(7, spreadsheet.getValueFromCell("A1"), DELTA);
    }

    @Test
    public void formulaWithCells() {
        Cell cellA1 = sheet.getCell("A1");
        Cell cellA2 = sheet.getCell("A2");
        Cell cellA3 = sheet.getCell("A3");
        cellA1.setNewContent(new NumericContent(15));
        cellA2.setNewContent(new NumericContent(5));
        cellA3.setNewContent(new NumericContent(10));
        Cell cellB2 = sheet.getCell("B2");
        cellB2.setNewContent(new FormulaContent("=A1 - A2 + A3", new DependencyHelper(spreadsheet, sheet, "B2")));
        assertEquals(20, spreadsheet.getValueFromCell("B2"), DELTA);
    }

    @Test
    public void formulaWithCellsAndNumbers() {
        Cell cellA1 = sheet.getCell("A1");
        Cell cellA2 = sheet.getCell("A2");
        Cell cellA3 = sheet.getCell("A3");
        cellA1.setNewContent(new NumericContent(15));
        cellA2.setNewContent(new NumericContent(5));
        cellA3.setNewContent(new NumericContent(10));
        Cell cellB2 = sheet.getCell("B2");
        cellB2.setNewContent(new FormulaContent("=A1 - 4 + A2 + 1 - A3", new DependencyHelper(spreadsheet, sheet, "B2")));
        assertEquals(7, spreadsheet.getValueFromCell("B2"), DELTA);
    }

    @Test
    public void formulaReferenceCellFromAnotherSheet() {
        Sheet sheet2 = new Sheet("2");
        spreadsheet.addSheet(sheet2);
        Cell cellA1 = sheet.getCell("A1");
        Cell cellB1 = sheet2.getCell("B1");
        cellB1.setNewContent(new FormulaContent("=!default.A1",new DependencyHelper(spreadsheet, sheet2, "B1")));
        cellA1.setNewContent(new NumericContent(5));
        assertEquals(5, spreadsheet.getValueFromCell("B1"), DELTA);
        assertEquals("", spreadsheet.getValueAsString("default", "B1"));
    }

}